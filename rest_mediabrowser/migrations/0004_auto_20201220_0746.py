# Generated by Django 3.1.1 on 2020-12-20 07:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rest_mediabrowser', '0003_auto_20201130_0405'),
    ]

    operations = [
        migrations.AlterField(
            model_name='node',
            name='media_type',
            field=models.CharField(choices=[('mediaimage', 'Image'), ('mediafile', 'File'), ('collection', 'Collection'), ('undefined', 'Undefined')], default='undefined', max_length=10),
        ),
    ]

from django.apps import AppConfig


class RestMediabrowserConfig(AppConfig):
    name = 'rest_mediabrowser'
